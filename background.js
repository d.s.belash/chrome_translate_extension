let date=new Date();
let day=("0" + (date.getDate())).slice(-2);
let month=("0" + (date.getMonth() + 1)).slice(-2);
let year=date.getFullYear();
date=day+'-'+month+'-'+year;

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        let gotWord;
        request.word?gotWord=request.word:null;
        let sourceLang;
        let targetLang;
        chrome.storage.local.get(['key'], function(result) {
            if(Object.keys(result).length !== 0) {
                sourceLang = result.key.from;
                targetLang = result.key.to;
            }
            let from=sender.url.indexOf('chrome-extension://');//лучше по id определять, ну да ладно
            from===-1?makeRequest(gotWord, true, sourceLang, targetLang):makeRequest(gotWord, false,  sourceLang, targetLang);
        });
    });

function makeRequest(word, display, sourceLang='en', targetLang='ru') {
    let translated;
    let xhr = new XMLHttpRequest();

    xhr.open('GET', 'https://translate.googleapis.com/translate_a/single?client=gtx&sl='+sourceLang+'&tl='+targetLang+'&dt=t&q='+word, true);

    xhr.send();

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4){
            if (xhr.status !== 200) {
                console.log( xhr.status );
            } else {
                chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
                    translated=JSON.parse(xhr.responseText);
                    if(translated[0][0][0]) {
                        insertToLocalStorage(translated[0][0][1], translated[0][0][0], date);
                        chrome.tabs.sendMessage(tabs[0].id, {translation: translated[0][0][0], display: display}, null);//"отправляем" перевод обратно в content-script

                    }
                });
            }
        }
    }
}



function insertToLocalStorage(word, translation, date) {
    if(localStorage.getItem(date)){
        let localValue =  JSON.parse(localStorage.getItem(date));
        localValue.push({ word: word,
            translation: translation,
            toDictionary: true,
        });
        localStorage.setItem(date, JSON.stringify(localValue));
    }
    else{
        let localValue=[];
        localValue.push({ word: word,
            translation: translation,
            toDictionary: true,
        });
        localStorage.setItem(date, JSON.stringify(localValue));
    }
    chrome.runtime.sendMessage({addedToTheStorage: true});
    // insertToStorageLocal(word, translation);//пока не надо, но может пригодиться
}

function sendOnServer(word, translation) {

}

function insertToStorageLocal(word, translation) {
    chrome.storage.local.get([date], function(result){
        let wordList=result[date];
        wordList.push({ word: word,
            translation: translation,
            toDictionary: true,
        });
        chrome.storage.local.set({[date]:wordList}, null);
    });
}


