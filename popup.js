let date=new Date();
let day=("0" + (date.getDate())).slice(-2);
let month=("0" + (date.getMonth() + 1)).slice(-2);
let year=date.getFullYear();
date=day+'-'+month+'-'+year;
document.addEventListener('DOMContentLoaded', ()=>{
    chrome.storage.local.get(['key'], function(result){
      if(result.key) {
        document.querySelector('#from').value = result.key.from;
        document.querySelector('#to').value = result.key.to;
      }
    });
    document.querySelector('#date').innerHTML=date;
    displayWords('#word-list', date);
});

document.querySelector('#add').onclick=(e)=>{
  e.preventDefault();
  let dictionaryWords=[];
  let wordList = JSON.parse(localStorage.getItem(date));
  wordList.map((word)=>{
    word.toDictionary?dictionaryWords.push(word):null;
  })
};

document.querySelector('#find').onclick=(e)=>{
    e.preventDefault();
    let date=document.querySelector('#date-input').value;
    let dateArr=date.split('-');
    let reversed=dateArr.reverse();
    let correctDate=reversed.join('-');
    document.querySelector('#date').innerHTML=correctDate;
    displayWords('#word-list', correctDate);
};


document.querySelector('.lang').onchange =(e)=>{
  setLang();
};

document.querySelector('#send-req').onclick=(e)=>{
    e.preventDefault();
    let word=document.getElementById('request').value;
    if(word) {
        chrome.runtime.sendMessage({word: word}, null);//для передачи данных в background.js
    }
};

// document.querySelector('#field').onclick=(e)=>{
//   let target = e.target;
//   if (target.tagName !== 'INPUT') return;
//   chrome.runtime.sendMessage({target: target.checked}, null);
// };


chrome.runtime.onMessage.addListener(
    (request)=> {
        if (request.addedToTheStorage) {
            window.location.reload();
        }
    }
);

function setLang() {
    chrome.storage.local.set({ "key":
            {
                "from": document.getElementById('from').value,
                "to":document.getElementById('to').value
            },
    }, null);
}

function displayWords(container, key) {
    let wrapper=document.querySelector(container);
    console.log(33, key);
    if(localStorage.getItem(key)) {
        let wordList = JSON.parse(localStorage.getItem(key));
        for(let i in wordList) {
            let li=document.createElement('li');
            li.innerHTML='<div>'+wordList[i].word+' - '+wordList[i].translation+'<input type="checkbox" checked='+wordList[i].toDictionary+' number='+i+'>'+'</div>';
            wrapper.appendChild(li);
        }
    }
}
