let coords;
window.ondblclick=addWord;
window.onkeydown=(event)=>{
    if((event.key.toLowerCase()==='q'||event.key.toLowerCase()==='й')&&event.ctrlKey){
        addWord();
}};

document.onclick=()=>{
    let div=document.querySelector('#translation-container');
    if(div)
    document.body.removeChild(div);
};

chrome.runtime.onMessage.addListener(
    function(request, sender, sendResponse) {
        if(request.display && request.translation) {
            translationWindow(coords, request.translation);
        }
    });

function addWord() {
    let txt = '';
    let word=(function getSelectionText(txt) {
        if (txt = window.getSelection) {
            txt = window.getSelection().toString();
        }
        return txt.trim();
    })(txt);
    if(word) {
        chrome.runtime.sendMessage({word: word}, null);//для передачи данных в background.js
    }
    coords=getSelectionCoords();
}

function getSelectionCoords() {
    let sel = document.selection, range;
    let x = 0, y = 0;
    if (sel) {
        if (sel.type !== "Control") {
            range = sel.createRange();
            range.collapse(true);
            x = range.boundingLeft;
            y = range.boundingTop+pageYOffset;
        }
    } else if (window.getSelection) {
        sel = window.getSelection();
        if (sel.rangeCount) {
            range = sel.getRangeAt(0).cloneRange();
            range.collapse(true);
            let rect = range.getClientRects()[0];
            if (rect) {
                x = rect.left;
                y = rect.top+pageYOffset;
            }
        }
    }
    return { x: x, y: y };
}

function translationWindow(coords, data) {
    let div = document.createElement('div');
    div.setAttribute('id','translation-container');
    div.style.cssText="    \n" +
        "    border: 1px solid black;\n" +
        "    background-color: yellow;\n" +
        "    padding: 5px 10px;\n" +
        "    box-shadow: 10px 10px 42px -10px rgba(0,0,0,.5);";
    div.innerHTML="<strong>" + data + "</strong>";
    div.style.position ='absolute';
    div.style.zIndex = '1000';
    div.style.top = coords.y-30+'px';
    div.style.left = coords.x-10+'px';
    document.body.appendChild(div);
}

//AIzaSyCysJXb1KhPkBUit5sZ1JlCCiRF72RAa6Q